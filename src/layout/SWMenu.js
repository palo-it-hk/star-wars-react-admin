import React from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { withRouter } from 'react-router-dom';
import {
  MenuItemLink,
  getResources,
} from 'react-admin';
import { withStyles } from '@material-ui/core/styles';
import { changeTheme } from '../actions/themeActions';
import UserIcon from '@material-ui/icons/Group';
import FilmIcon from '@material-ui/icons/Theaters';
import SpaceshipIcon from '@material-ui/icons/FlightTakeoff';
import NavigateIcon from '@material-ui/icons/AddLocation';
import SpeciesIcon from '@material-ui/icons/Android';
import VehiclesIcon from '@material-ui/icons/AirportShuttle';

const styles = {
  lightSide: {
    background: 'white',
    color: '#2719C7',
  },
  darkSide: {
    color: 'red',
    background: 'black',
  },
}

const getIcon = (resource) => {
  switch(resource) {
    case 'people':
      return <UserIcon />
    case 'films':
      return <FilmIcon />
    case 'starships':
      return <SpaceshipIcon />
    case 'planets':
      return <NavigateIcon />
    case 'species':
      return <SpeciesIcon />
    case 'vehicles':
      return <VehiclesIcon />
    default:
      return <UserIcon />
  }
}

const SWMenu = ({
  resources,
  classes,
  theme,
}) => {
    return (
      <div>
        {resources.map((resource, i) => {
          return (
            <MenuItemLink
              className={ theme === 'light' ? classes.lightSide : classes.darkSide }
              key={i}
              to={`/${resource.name}`}
              primaryText={resource.name}
              leftIcon={getIcon(resource.name)}
            />
          )
        })}
      </div>
    );
};


const mapStateToProps = state => ({
  resources: getResources(state),
  theme: state.theme,
});

const enhance = compose(
    connect(mapStateToProps, { changeTheme }),
    withStyles(styles)
);

export default withRouter(enhance(SWMenu));
