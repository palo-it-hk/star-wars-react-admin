import React from 'react';

const customStyle = {
    height: "30px",
}

const LightSideLogo = props => (
    <img
        style={customStyle}
        src="/assets/images/Rebel_Alliance_logo.svg.png"
        alt="rebel alliance logo"
    />
);

export default LightSideLogo;
