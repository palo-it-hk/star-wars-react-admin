import React from 'react';

import { Layout } from 'react-admin';
import SWAppBar from './SWAppBar';
// import MySidebar from './MySidebar';
import SWMenu from './SWMenu';
// import MyNotification from './MyNotification';

const SWLayout = props => {
    return <Layout
        {...props}
        appBar={SWAppBar}
        menu={SWMenu}
    />;
}

export default SWLayout;
