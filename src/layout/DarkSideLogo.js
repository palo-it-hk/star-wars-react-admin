import React from 'react';

const customStyle = {
    height: "30px",
}

const DarkSideLogo = props => (
    <img
        style={customStyle}
        src="/assets/images/svg-cricut-star-wars-1.png"
        alt="rebel alliance logo"
    />
);

export default DarkSideLogo;
