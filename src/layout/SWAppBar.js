import React from 'react';
import { AppBar } from 'react-admin';
import { changeTheme } from '../actions/themeActions';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import { connect } from 'react-redux';
import compose from 'recompose/compose';

import LightSideLogo from './LightSideLogo';
import DarkSideLogo from './DarkSideLogo';

const styles = {
    title: {
        flex: 1,
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
    },
    lightSide: {
        color: '#2719C7',
        background: 'white',
    },
    darkSide: {
        color: 'red',
        background: 'black',
    },
    spacer: {
        flex: 1,
    },
};

const SWAppBar = ({
    classes,
    theme,
    changeTheme,
    ...props
}) => (
    <AppBar {...props } className={theme === 'light' ? classes.lightSide : classes.darkSide}>
        <Typography
            variant="title"
            color="inherit"
            className={classes.title}
            id="react-admin-title"
        />
        <span className={classes.spacer} />
        <button onClick={() => changeTheme('light')} hidden={theme === 'light' }>
            Light Side
            <LightSideLogo/>
        </button>
        <button onClick={() => changeTheme('dark')} hidden={theme === 'dark' } >
            Dark Side
            <DarkSideLogo/>
        </button>
    </AppBar>
)

const mapStateToProps = state => ({
    theme: state.theme,
});

const enhance = compose(
    connect(mapStateToProps, { changeTheme }),
    withStyles(styles)
);

export default enhance(SWAppBar);
