import React from 'react';
import {
  Admin,
  Resource,
  ListGuesser,
  // EditGuesser,
  // fetchUtils,
} from 'react-admin';
// import { PostList, PostEdit, PostCreate } from './posts';
// import { UserList } from './users';
import Dashboard from './layout/Dashboard';
import { StarshipList } from './ships/ships';
// import jsonServerProvider from 'ra-data-json-server';

import { PersonList } from './people/people';
import authProvider from './auth/authProvider';
import dataProvider from './dataProvider';
import SWLayout from './layout/SWLayout';
// Post Tutorial
// import simpleRestProvider from 'ra-data-simple-rest';
// import addUploadFeature from './addUploadFeature';
// const uploadCapableDataProvider = addUploadFeature(dataProvider);
import themeReducer from './themeReducer';

const App = () => (
  <Admin
    customReducers={{ theme: themeReducer }}
    appLayout={SWLayout}
    dashboard={Dashboard}
    authProvider={authProvider}
    dataProvider={dataProvider}
    >
    <Resource
      name="people"
      list={PersonList}
    />
    <Resource
      name="films"
      list={ListGuesser}
    />
    <Resource
      name="planets"
      list={ListGuesser}
    />
    <Resource
      name="starships"
      list={StarshipList}
    />
    <Resource
      name="species"
      list={ListGuesser}
    />
    <Resource
      name="vehicles"
      list={ListGuesser}
    />

    {/* <Resource name="posts" list={PostList} edit={PostEdit} create={PostCreate} icon={PostIcon} />
    <Resource name="users" list={UserList} icon={UserIcon} /> */}
  </Admin>
);

export default App;
