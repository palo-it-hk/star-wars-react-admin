import React from 'react';
import { List, Datagrid, TextField, DateField, UrlField, Filter, TextInput } from 'react-admin';

const TitleElem = (props) => {
  return (
    <span style={{ display: 'flex', justifyContent: 'center' }}>
      testing title element in center
    </span>
  )
}

const ShipFilter = (props) => {
  console.log("props", props)
  return (
    <Filter {...props}>
      <TextInput label="Search" source="q" alwaysOn />
      <TextInput label="name" source="name" defaultValue="Executor" />
    </Filter>
  )
};

export const StarshipList = props => (
  <List {...props} title={<TitleElem />} filter={{ crew: true }}>
    <Datagrid rowClick="edit">
      <TextField source="name" />
      <TextField source="model" />
      <TextField source="manufacturer" />
      <TextField source="cost_in_credits" />
      <TextField source="length" />
      <TextField source="max_atmosphering_speed" />
      <TextField source="crew" />
      <TextField source="passengers" />
      <TextField source="cargo_capacity" />
      <TextField source="consumables" />
      <TextField source="hyperdrive_rating" />
      <DateField source="MGLT" />
      <TextField source="starship_class" />
      <TextField source="pilots" />
      <TextField source="films" />
      <DateField source="created" />
      <DateField source="edited" />
      <UrlField source="url" />
      <TextField source="id" />
    </Datagrid>
  </List>
);