import React from 'react';
import {
  List,
  Datagrid,
  TextField,
  DateField,
} from 'react-admin';

export const PersonList = props => (
  <List {...props}>
      <Datagrid rowClick="edit">
          <TextField source="name" />
          <TextField source="height" />
          <TextField source="mass" />
          <TextField source="hair_color" />
          <TextField source="skin_color" />
          <TextField source="eye_color" />
          <TextField source="birth_year" />
          <TextField source="gender" />
          <TextField source="homeworld" />
          {/* <TextField source="films" />
          <TextField source="species" />
          <TextField source="vehicles" />
          <TextField source="starships" /> */}
          <DateField source="created" />
          <DateField source="edited" />
          {/* <UrlField source="url" /> */}
          <TextField source="id" />
      </Datagrid>
  </List>
);
